# -*- coding: utf8 -*-
import sys, os, uuid
     
def main(args):

    if not args:
        print('How many?')
        return

    file_count = int(args[0])
    print('Creating {} files'.format(file_count))

    for i in range(1, file_count + 1):
        fp = os.path.join('c:\\Projects\\dir\\in\\', 'in_{}.in'.format(i))
        with open(fp, 'w') as output:
            for j in range(1, 101):
                print('{}: somedata{}'.format(j, j), file=output)

if __name__ == "__main__":
    main(sys.argv[1:])
